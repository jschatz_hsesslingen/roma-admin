package edu.ltu.romaadmin;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class AddTransactionActivity extends AppCompatActivity {

    public static final String CustomerID = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_transaction);

        String customerID = (String)getIntent().getExtras().get(CustomerID);
        TextView userID = findViewById(R.id.userID);
        userID.setText(customerID);


    }

    public void onButtonAddClick(View view){
        TextView editCustomerID = findViewById(R.id.userID);
        String customerID = editCustomerID.getText().toString();

        Spinner spbranch = (Spinner)findViewById(R.id.spinnerBranch);
        String branch = spbranch.getSelectedItem().toString();

        TextView tvvalue = findViewById(R.id.editValue);
        String value = tvvalue.getText().toString();

        TextView tvfactor = findViewById(R.id.editFactor);
        Integer factor = Integer.parseInt(tvfactor.getText().toString());

        //The customer gets one point per each 10 cents he spends * points factor
        String points = String.valueOf(Integer.parseInt(value)/10*factor);


        //Write new data in database with current timestamp
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("purchases").child("customerID").child(customerID);

        Map<String, String> userData = new HashMap<String, String>();

        userData.put("branch", branch);
        userData.put("type", "purchase");
        userData.put("value", value);
        userData.put("points", points);

        //get the current Timestamp to create new Node in Database
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
        String current = sdf.format(new Date());

        myRef.child(current).setValue(userData);
        Log.w("FIREBASE_WRITE", current);
        Log.w("FIREBASE_WRITE", userData.toString());

        tvvalue.setText("");
        Toast.makeText(this, "Added purchase to Database",
                Toast.LENGTH_LONG).show();
        finish();
    }



}
